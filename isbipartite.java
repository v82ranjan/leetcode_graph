class Solution {
    public boolean isBipartite(int[][] graph) {
        int n = graph.length;
        int[] marked = new int[n];
        Arrays.fill(marked, -1);
        Queue<Integer> queue = new LinkedList();
        for (int start = 0; start < n; ++start) {
            if (marked[start] == -1) {
                
                queue.offer(start);
                marked[start] = 0;

                while (!queue.isEmpty()) {
                    Integer node = queue.poll();
                    for (int gn: graph[node]) {
                        if (marked[gn] == -1) {
                            queue.offer(gn);
                           if(marked[node]==1)
                               marked[gn]=0;
                            else
                                marked[gn]=1; 
                        } else if (marked[gn] == marked[node]) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
}

/* Problem 

Given an undirected graph, return true if and only if it is bipartite.

Recall that a graph is bipartite if we can split it's set of nodes into two independent subsets A and B such that every edge in the graph has one node in A and another node in B.

The graph is given in the following form: graph[i] is a list of indexes j for which the edge between nodes i and j exists.  Each node is an integer between 0 and graph.length - 1.  There are no self edges or parallel edges: graph[i] does not contain i, and it doesn't contain any element twice.

Example 1:
Input: [[1,3], [0,2], [1,3], [0,2]]
Output: true
Explanation: 
The graph looks like this:
0----1
|    |
|    |
3----2
We can divide the vertices into two groups: {0, 2} and {1, 3}.
*/